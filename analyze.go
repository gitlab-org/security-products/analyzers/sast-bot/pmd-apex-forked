package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v5/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

const (
	pathPmd    = "/pmd/bin/run.sh"
	pathOutput = "/tmp/pmd-output.xml"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(_ *cli.Context, repositoryPath string, _ *ruleset.Config) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = repositoryPath
		cmd.Env = os.Environ()
		return cmd
	}

	args := []string{
		"pmd",
		"-no-cache",
		"--failOnViolation", "false",
		"-dir", ".",
		"-format", "xml",
		"-rulesets", "category/apex/security.xml",
		"-reportfile", pathOutput,
	}

	cmd := setupCmd(exec.Command(pathPmd, args...))
	log.Debug(cmd.String())
	// log all output from the pmd command using the Writer() default `info` level
	streamingInfoLogger := log.StandardLogger().Writer()
	defer streamingInfoLogger.Close()
	cmd.Stderr = streamingInfoLogger

	if err := cmd.Run(); err != nil {
		log.Errorf("command exec failure \n command: %q\n error: %v", cmd.String(), err)
		return nil, err
	}

	return os.Open(pathOutput)
}

func loadRulesetConfig(projectPath string) (*ruleset.Config, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	return ruleset.Load(rulesetPath, metadata.AnalyzerID, log.StandardLogger())
}
