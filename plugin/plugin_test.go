package plugin

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMatch(t *testing.T) {
	var tcs = []struct {
		name, path string
		want       bool
	}{
		{"valid match", "app/src/package.xml", true},
		{"always match sfdx-project.json filename", "sfdx-app/sfdx-project.json", true},
		{"class does not match", "otherclass/src/package.xml", false},
		{"broken package", "broken/src/package.xml", false},
		{"package does not contain a class", "noclass/src/package.xml", false},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			path := filepath.Join("../test/fixtures/", tc.path)
			fileInfo, err := os.Stat(path)
			require.NoError(t, err)

			got, err := Match(path, fileInfo)
			require.NoError(t, err)
			require.Equal(t, tc.want, got)

		})
	}
}
