# pmd-apex analyzer

pmd-apex performs SAST scanning on repositories containing code written for [Apex](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_intro_what_is_apex.htm) projects.

The analyzer wraps [pmd](https://pmd.github.io), a static code analyzer utilizing its [Apex security rules](https://docs.pmd-code.org/pmd-doc-6.22.0/pmd_rules_apex_security.html), and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## Using the analyzer

This analyzer looks for two specific files to determine if it should run against a repository.

* `sfdx-project.json`
  * If this file exists, the analyzer will match and scan the project.
* `package.xml`
  * If this file contains any nodes containing `<name>ApexClass</name>`, the analyzer will match and scan the project.

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
