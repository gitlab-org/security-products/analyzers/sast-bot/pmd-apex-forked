# pmd-apex analyzer changelog

## v5.11.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.3.0` => [`v5.5.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.5.0)] (!153)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.1` => [`v3.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.3.1)] (!153)

## v5.10.0
- upgrade `github.com/stretchr/testify` version [`v1.9.0` => [`v1.10.0`](https://github.com/stretchr/testify/releases/tag/v1.10.0)] (!152)

## v5.9.0
- always stream output from scanner using `info` log level (!149)

## v5.8.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.4` => [`v2.27.5`](https://github.com/urfave/cli/releases/tag/v2.27.5)] (!147)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.3.0` => [`v3.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.4.0)] (!147)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.0` => [`v3.2.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.1)] (!147)

## v5.7.0
- Add support for disabling predefined rules via custom rulesets (!146)

## v5.6.0
- Add non-root user (gitlab, uid=1000, gid=1000) (!142)

## v5.5.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.3` => [`v2.27.4`](https://github.com/urfave/cli/releases/tag/v2.27.4)] (!144)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.1` => [`v3.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.0)] (!144)

## v5.4.1
- handle and remove inline comment in package.xml file (!141)

## v5.4.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.3` => [`v2.27.4`](https://github.com/urfave/cli/releases/tag/v2.27.4)] (!139)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.2.1` => [`v5.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.3.0)] (!139)

## v5.3.0
- upgrade `golang.org/x/crypto` version [`v0.18.0` => `v0.23.0`] (!135)
- upgrade `golang.org/x/net` version [`v0.20.0` => `v0.25.0`] (!135)

## v5.2.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.2` => [`v2.27.3`](https://github.com/urfave/cli/releases/tag/v2.27.3)] (!131)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.0` => [`v3.1.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.1.1)] (!131)

## v5.1.0
- Update `command` package from `v2.4.0` to `v3.1.0` (!125)
- Update `common` package from `v3.2.3` to `v3.3.0` (!125)
- Update `report` package from `v4.4.0` to `v5.2.1` (!125)
- Update `ruleset` package from `v2.0.9` to `v3.1.0` (!125)
- Update version of `pmd-apex` module from `v2` to `v5` (!125)

## v5.0.2
- update `go` version to `v1.22.3` (!129)

## v5.0.1
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!128)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.2.0` => [`v2.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.4.0)] (!128)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.2` => [`v4.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.4.0)] (!128)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!128)

## v5.0.0
- Bump to next major version (!127)

## v4.1.9
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!124)

## v4.1.8
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!123)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!123)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!123)

## v4.1.7
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!121)
- upgrade `github.com/go-git/go-git/v5` version [`v5.4.2` => [`v5.11.0`](https://github.com/go-git/go-git/releases/tag/v5.11.0)] (!121)
- upgrade go to v1.19 (!121)

## v4.1.6
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!120)

## v4.1.5
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!119)

## v4.1.4
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!118)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!118)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!118)

## v4.1.3
- upgrade [`PMD`](https://github.com/pmd/pmd) version [`6.54.0` => [`6.55.0`](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.55.0)] (!110)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!114)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!114)

## v4.1.2
- upgrade `github.com/urfave/cli/v2` version [`v2.25.5` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!110)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!110)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!110)

## v4.1.1
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (gitlab-org/security-products/analyzers/pmd-apex!109)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (gitlab-org/security-products/analyzers/pmd-apex!109)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (gitlab-org/security-products/analyzers/pmd-apex!109)

## v4.1.0
- Update `ruleset` module to `v2.0.2` to support loading remote Custom Rulesets (!108)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (!108)

## v4.0.1
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!107)

## v4.0.0
- Bump to next major version (!105)
- Upgrade report schema to v15 (!105)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!105)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!105)

## v3.3.4
- upgrade `github.com/urfave/cli/v2` version [`v2.24.3` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!103)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!103)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!103)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!103)

## v3.3.3
- upgrade [`PMD`](https://github.com/pmd/pmd) version [`6.53.0` => [`6.54.0`](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.54.0)] (!101)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.24.3`](https://github.com/urfave/cli/releases/tag/v2.24.3)] (!101)

## v3.3.2
- upgrade [`PMD`](https://github.com/pmd/pmd) version [`6.50.0` => [`6.53.0`](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.53.0)] (!100)
  - Deprecates two rules
  - Improves the `UnusedPrivateField` Java rule
  - Fixes several issues
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!100)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!100)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.2` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!100)

## v3.3.1
- Ensure `git` is installed in the Docker image (!96)

## v3.3.0
- upgrade [`PMD`](https://github.com/pmd/pmd) version [`6.49.0` => [`6.50.0`](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.50.0)] (!94)
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!94)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!94)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.0` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!94)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.15.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!94)

## v3.2.2
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!93)

## v3.2.1
- upgrade [`PMD`](https://github.com/pmd/pmd) version [`6.48.0` => [`6.49.0`](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.49.0)] (!92)

## v3.2.0
- upgrade [`PMD`](https://github.com/pmd/pmd) version [`6.47.0` => [`6.48.0`](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.48.0)] (!91)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!91)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!91)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!91)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!91)

## v3.1.2
- Update command version (!90)

## v3.1.1
- Updated PMD to [v6.47.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.47.0) (!87)
  - [core] address common-io path traversal vulnerability (CVE-2021-29425)

## v3.1.0
- Upgrade core analyzer dependencies (!88)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.1
- Updated PMD to [v6.45.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.45.0) (!86)

## v3.0.0
- Bump to next major version `v3.0.0` (!85)

## v2.12.15
- Update `command` to v1.7.0 (!81)

## v2.12.14
- Updated PMD to [v6.44.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.44.0) (!79)

## v2.12.13
- Updated PMD to [v6.43.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.43.0) (!78)
- Update go dependencies (!78)

## v2.12.12
- Update go dependencies (!78)
  - Support ruleset overrides

## v2.12.11
- Updated PMD to [v6.42.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.42.0) (!77)

## v2.12.10
- Remove `pmd-java` submodule entirely (!75)

## v2.12.9
- Enable `log4j2.formatMsgNoLookups` by default (!74)

## v2.12.8
- Updated PMD to [v6.40.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.40.0) (!72)
  - The Apex language support has been bumped to version 54.0
  - Various improvements and bugfixes for Apex ruleset

## v2.12.7
- Upgrade go to v1.17 (!71)

## v2.12.6
- Update PMD to [v6.39.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.39.0) (!69)

## v2.12.5
- Update PMD to [v6.38.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.38.0) (!68)
  - fix: SOQL performed in a for-each loop doesn't trigger ApexCRUDViolationRule
  - fix: ApexCRUDViolationRule maintains state across files
  - fix: ApexCRUDViolationRule - add super call

## v2.12.4
- Update PMD to [v6.36.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.36.0) (!66)
  - Improved Incremental Analysis performance

## v2.12.3
- Update PMD to [v6.35.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.35.0) (!61)
  - fix: [[apex] Correct findBoundary when traversing AST](https://github.com/pmd/pmd/pull/3243)

## v2.12.2
- Update PMD to [v6.34.0](https://github.com/pmd/pmd/releases/tag/pmd_releases%2F6.34.0) (!58)
  - The Apex rule `ApexCRUDViolation` does not ignore getters anymore and also flags
  - fix: `ApexCRUDViolationRule` fails to report CRUD violation on COUNT() queries
  - fix: `ApexCRUDViolationRule` false-negative on non-VF getter
  - `ApexCRUDViolationRule`: Do not assume method is VF getter to avoid CRUD checks
  - `ApexCRUDViolation`: COUNT is indeed CRUD checkable since it exposes data (false-negative)

## v2.12.1
- Update PMD to v6.33.0 (!57)

## v2.12.0
- Update report dependency in order to use the report schema version 14.0.0 (!54)

## v2.11.2
- Update PMD to v6.32.0 (!53)

## v2.11.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!51)

## v2.11.0
- Update base image to adoptopenjdk/openjdk15:alpine (!50)
- Update PMD to v6.30.0 (!50)

## v2.10.0
- Update common to v2.22.0 (!49)
- Update urfave/cli to v2.3.0 (!49)

## v2.9.0
- Update PMD to v6.29.0 (!48)
- Update golang dependencies logrus to v1.6.0, cli to v1.22.5 (!48)

## v2.8.0
- Update common and enabled disablement of rulesets (!47)

## v2.7.1
- Update golang dependencies (!41)

## v2.7.0
- Upgrade PMD to 6.27.0 (!39)
- Update golang dependencies (!39)

## v2.6.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!37)

## v2.5.1
- Update golang to v1.15 (!34)
- Bump PMD to 6.26.0

## v2.5.0
- Add scan object to report (!30)

## v2.4.3
- Dedupe report findings (!28)

## v2.4.2
- Safe handle plugin.Match file closer (!29)

## v2.4.1
- Bump PMD to 6.25.0 (!24)

## v2.4.0
- Switch to the MIT Expat license (!23)

## v2.3.1
- Update Debug output to give a better description of command that was ran (!21)

## v2.3.0
- Update logging to be standardized across analyzers (!20)

## v2.2.4
- Bump PMD to 6.24.0 (!18)

## v2.2.3
- Remove `location.dependency` from the generated SAST report (!17)

## v2.2.2
- Bump PMD to 6.23.0, Docker image to Java 13 (!16)

## v2.2.1
- Bump PMD to 6.22.0 (!14)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!11)

## v2.1.0
- Add support for custom CA certs (!9)

## v2.0.1
- Bump PMD to 6.17.0

## v2.0.0
- Initial release
