FROM golang:1.22.3 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM adoptopenjdk/openjdk15:alpine

RUN apk update && \
    apk add --no-cache zip bash git && \
    apk upgrade --no-cache

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-6.55.0}

RUN wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F$SCANNER_VERSION/pmd-bin-$SCANNER_VERSION.zip -O pmd.zip && \
    unzip -n pmd.zip && \
    rm -f pmd.zip && \
    mv pmd-bin-$SCANNER_VERSION /pmd && \
    # Remove pmd-java explicitly as unneeded module
    rm /pmd/lib/pmd-java-${SCANNER_VERSION}.jar

# Install analyzer
COPY --from=build /go/src/app/analyzer /

RUN addgroup -g 1000 gitlab && adduser -u 1000 -G gitlab -S -D gitlab 
# Uncomment the following to run as non-root
# USER gitlab
ENTRYPOINT []
CMD ["/analyzer", "run"]
